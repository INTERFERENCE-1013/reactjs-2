import React, {useEffect, useState} from 'react'
import '../sass/views/DolarInfo.sass'
import axios from 'axios';
import {Bar} from 'react-chartjs-2';
import {BsArrowLeftRight, BsGraphDown, BsGraphUp} from "react-icons/bs"
import {GoGraph} from "react-icons/go"

function DolarInfo () {

    useEffect(() => {
        getDailyDolar()
    }, [])

    const [dailyDollarValue, setDailyDollarValue] = useState([])
    const [dollarValue1, setDollarValue1] = useState(0)
    const [dollarValue2, setDollarValue2] = useState(0)
    const [minimumDate, setMinimumDate] = useState("")
    const [higherDollar, setHigherDollar] = useState(0)
    const [lowerDollar, setlowerDollar] = useState(0)
    const [message, setMessage] = useState("")

    //Establece el valor del dólar del día, también queda incluído en el gráfico
    const getDailyDolar = async () => {
        await axios.get(`https://mindicador.cl/api`)
        .then(res => {
            const data = res.data.dolar.valor;
            setDailyDollarValue(data);
        })
    }

    //Obtiene el valor del dólar en la fecha seleccionada, también establece la fecha mínima
    //del segundo input date, en caso de no haber valor en la fecha arrojará un alert
    const getInitialDolar = async (e) => {
        const date = e.target.value
        setMinimumDate(e.target.value)
        const year = date.substr(0,4);
        const month = date.substr(5,2);
        const day = date.substr(8,2);
        await axios.get(`https://mindicador.cl/api/dolar/${day}-${month}-${year}`)
        .then(res => {
            const data = res.data.serie[0].valor;
            setDollarValue1(data);
            calculateDolar(data, 1);
            setMessage("")
        }).catch( error => {
            setMessage("Fecha A: No hay valor del dólar en esta fecha")
        })
    }

    //Obtiene el valor del dólar en la fecha seleccionada, en caso de no haber valor en la
    //fecha arrojará un alert
    const getFinalDolar = async (e) => {
        const date = e.target.value
        const year = date.substr(0,4);
        const month = date.substr(5,2);
        const day = date.substr(8,2);
        await axios.get(`https://mindicador.cl/api/dolar/${day}-${month}-${year}`)
        .then(res => {
            const data = res.data.serie[0].valor;
            setDollarValue2(data);
            calculateDolar(data, 2);
            setMessage("")
        }).catch( error => {
            setMessage("Fecha B: No hay valor del dólar en esta fecha")
        })
    }

    //Método que calcula el mayor y menor valor del dólar según las fechas seleccionadas
    const calculateDolar = async (value) => {
        if(value > lowerDollar){
            if(lowerDollar > higherDollar){
                setHigherDollar(lowerDollar)
            }
            setHigherDollar(value)
            if(lowerDollar === 0){
                setlowerDollar(value)
            }
        }else if(value === higherDollar){
            setlowerDollar(value)
        }else{
            setHigherDollar(lowerDollar)
            setlowerDollar(value)
        }
    }

    //Datos del gráfico
    const data = {
        labels: ["Fecha A: $" + dollarValue1, "Fecha B: $" + dollarValue2, "Valor del dia: $" + dailyDollarValue],
        datasets: [{
            label:'Valor',
            backgroundColor: 'rgba(0,0,0,1)',
            borderColor: 'black',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(0,0,0,1)',
            hoverBorderColor: '#FF0000',
            data: [dollarValue1, dollarValue2, dailyDollarValue]
        }]
    }

    return (
        <div className="container">
            <div className="row mt-4">
                <div className="col-12 text-center">
                    <div className="row">
                        <div className="col-12 col-md-6 text-center">
                            <div className="card bg-light">
                                <div className="card-body">
                                    <h5 className="card-title">Compara valores<GoGraph color="teal" size="40" className="ml-2"/></h5>
                                    <p className="card-text">Ingresa una fecha de inicio y término, tendrás un promedio de los valores obtenidos, también el mayor y menor valor</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-md-6 text-center my-auto">
                            <div className="row">
                                <div className="col-12 text-center my-2">
                                    <div className="input-group input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="inputGroup-sizing-lg">Fecha A</span>
                                        </div>
                                        <input type="date" name="fechaInicial" onChange={getInitialDolar} className="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg"/>
                                        <div className="input-group-append">
                                            <span className="input-group-text">$</span>
                                            <span className="input-group-text">{dollarValue1}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 text-center my-2">
                                    <div className="input-group input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="inputGroup-sizing-lg">Fecha B</span>
                                        </div>
                                        <input type="date" min={minimumDate} name="fechaInicial" onChange={getFinalDolar} className="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg"/>
                                        <div className="input-group-append">
                                            <span className="input-group-text">$</span>
                                            <span className="input-group-text">{dollarValue2}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p className="font-weight-bold text-danger my-n2">{message}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row mt-3" align="center">
                <div className="col-12 col-lg-4 text-center">
                    <div className="row">
                        <div className="col-12">
                            <div className="card bg-light my-1">
                                <div className="row no-gutters">
                                    <div className="col-4 my-auto">
                                        <BsArrowLeftRight color="teal" size="80"/>
                                    </div>
                                    <div className="col-8">
                                        <div className="card-body">
                                            <h5 className="card-title">Promedio</h5>
                                            <h3 className="card-text">${((higherDollar+lowerDollar)/2).toFixed(2)}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="card bg-light my-1">
                                <div className="row no-gutters">
                                    <div className="col-4 my-auto">
                                        <BsGraphUp color="green" size="80"/>
                                    </div>
                                    <div className="col-8">
                                        <div className="card-body">
                                            <h5 className="card-title">Mayor valor</h5>
                                            <h3 className="card-text">${higherDollar}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="card bg-light my-1">
                                <div className="row no-gutters">
                                    <div className="col-4 my-auto">
                                        <BsGraphDown color="red" size="80"/>
                                    </div>
                                    <div className="col-8">
                                        <div className="card-body">
                                            <h5 className="card-title">Menor valor</h5>
                                            <h3 className="card-text">${lowerDollar}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 col-lg-8 my-auto mx-auto">
                    <Bar data={data}/>
                </div>
            </div>
        </div>
    );
}

export default DolarInfo