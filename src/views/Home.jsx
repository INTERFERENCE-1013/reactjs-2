import React, {useState, useEffect} from 'react'
import {FcCurrencyExchange} from "react-icons/fc"
import axios from 'axios';

const Inicio = () => {

    useEffect(() => {
        getDailyDolar()
        getDailyEuro()
        getDailyUF()
        getDailyUTM()
    }, [])

    const [dailyDollarValue, setDailyDollarValue] = useState(0)
    const [dailyEuroValue, setDailyEuroValue] = useState(0)
    const [dailyUFValue, setDailyUFValue] = useState(0)
    const [dailyUTMValue, setDailyUTMValue] = useState(0)

    const getDailyDolar = async () => {
        await axios.get(`https://mindicador.cl/api`)
        .then(res => {
            const data = res.data.dolar.valor;
            setDailyDollarValue(data);
        })
    }

    const getDailyEuro = async () => {
        await axios.get(`https://mindicador.cl/api`)
        .then(res => {
            const data = res.data.euro.valor;
            setDailyEuroValue(data);
        })
    }

    const getDailyUF = async () => {
        await axios.get(`https://mindicador.cl/api`)
        .then(res => {
            const data = res.data.uf.valor;
            setDailyUFValue(data);
        })
    }

    const getDailyUTM = async () => {
        await axios.get(`https://mindicador.cl/api`)
        .then(res => {
            const data = res.data.utm.valor;
            setDailyUTMValue(data);
        })
    }

    return (
        <div className="jumbotron text-center">
            <FcCurrencyExchange size="150"/>
            <p className="lead">Consulta detalles de los valores al día</p>
            <div className="row">
                <div className="col-sm-12 col-lg-3">
                    <div className="card">
                        <h5 className="card-header bg-dark text-white">Dolar</h5>
                        <div className="card-body">
                            <h1 className="card-title">${dailyDollarValue}</h1>
                        </div>
                    </div>
                </div>
                <div className="col-sm-12 col-lg-3">
                    <div className="card">
                        <h5 className="card-header bg-dark text-white">Euro</h5>
                        <div className="card-body">
                            <h1 className="card-title">${dailyEuroValue}</h1>
                        </div>
                    </div>
                </div>
                <div className="col-sm-12 col-lg-3">
                    <div className="card">
                        <h5 className="card-header bg-dark text-white">UF</h5>
                        <div className="card-body">
                            <h1 className="card-title">${dailyUFValue}</h1>
                        </div>
                    </div>
                </div>
                <div className="col-sm-12 col-lg-3">
                    <div className="card">
                        <h5 className="card-header bg-dark text-white">UTM</h5>
                        <div className="card-body">
                            <h1 className="card-title">${dailyUTMValue}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <hr className="mt-4"/>
            <p>Ver mas información en MINDICADOR</p>
            <a className="btn btn-dark btn-lg" href="https://mindicador.cl/" role="button">Más información</a>
        </div>
    );
}

export default Inicio