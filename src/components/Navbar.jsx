import React from 'react'
import DolarInfo from '../views/DolarInfo';
import Home from '../views/Home';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import {RiExchangeDollarLine} from "react-icons/ri"

const Navbar = () => {
    return (
        <Router>
            <div className="inner">
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <a className="navbar-brand" href="/"><RiExchangeDollarLine size="40" color="green"/><span className="navbar-text">INDICADORES</span></a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <a href="/informacion-dolar" className="nav-link">Dolar</a>
                            </li>
                            <li className="nav-item">
                                <a href="/" className="nav-link disabled">Euro</a>
                            </li>
                            <li className="nav-item">
                                <a href="/" className="nav-link disabled">UTM</a>
                            </li>
                            <li className="nav-item">
                                <a href="/" className="nav-link disabled">UF</a>
                            </li>
                        </ul>
                        <span className="navbar-text">
                            Datos proporcionados por <a href="https://mindicador.cl/">MINDICADOR</a>
                        </span>
                    </div>
                </nav>
            </div>
            <Switch>
                <Route path="/informacion-dolar">
                    <DolarInfo/>
                </Route>
                <Route path="/" exact>
                    <Home/>
                </Route>
            </Switch>
        </Router>
    );
}

export default Navbar